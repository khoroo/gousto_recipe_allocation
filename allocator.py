#!/usr/bin/env python3

"""
Gousto recipe allocator algorithm

Given the path of orders JSON and stocks JSON,
if there is a feasible solution given the constraints outlined,
return True - otherwise False

Details as to implementation can be found in `presentation.pdf`

Example::
    $ python3 allocator.py tests/test_true_small__orders.json tests/test_true_small__stocks.json
Todo:
    * add logging - where/why are we failing
    * refactor code

by Robert Sparks
hire@rsparks.xyz

23/11/20
"""

from collections import defaultdict
import json
import numpy as np
import sys

# global mappings for JSON strings in spec to integers
DIET_MAP = {"vegetarian": 1, "gourmet": 0}
RECIPE_MAP = {"four_recipes": 4, "three_recipes": 3, "two_recipes": 2}
PORTION_MAP = {"four_portions": 4, "two_portions": 2}


def read_json(path):
    """
    Function `read_json` to load JSON file to Python dictionary

    Args:
        path (str): path of the JSON file to be read as dictionary

    Returns:
        dump (dict): dictionary of JSON file contents
    """
    try:
        with open(path, "r") as f:
            dump = json.load(f)
            return dump
    except FileNotFoundError as e:
        raise FileNotFoundError("JSON file not found, exception -", e)
    except json.JSONDecodeError as e:
        raise json.JSONDecodeError("Invalid JSON file, exception -", e)


def parse_orders(orders):
    """
    Function `parse_orders` to parse order dictionary array into flat data structures,
    of different diets

    Args:
        orders (dict): dict of orders json, output of read_json function

    Returns:
        data (defaultdict(list)): order data flattened as dict of lists

    Raises:
        ValueError: If any of the data is not recognised from
                    global dictionaries
    """
    data = defaultdict(list)
    try:
        for diet_str, recipes_dict in orders.items():
            is_veg = DIET_MAP[diet_str]

            for recipes_str, portion_dict in recipes_dict.items():
                recipes = RECIPE_MAP[recipes_str]

                for portion_str, num in portion_dict.items():
                    portion = PORTION_MAP[portion_str]

                    if type(num) != int:
                        raise ValueError(
                            "JSON has non int order value for",
                            diet_str,
                            recipes_str,
                            portion_str,
                        )
                    if is_veg:
                        data["veg_r"].append(recipes)
                        data["veg_p"].append(portion)
                        data["veg_n"].append(num)
                    else:
                        data["gourmet_r"].append(recipes)
                        data["gourmet_p"].append(portion)
                        data["gourmet_n"].append(num)

    except KeyError as e:
        # dictionaries ensure theres nothing unexpected in JSON
        raise ValueError("Invalid orders JSON schema, exception -", e)

    return data


def parse_stocks(stocks):
    """
    Function `parse_stocks` to parse dictionary of json dump_json into flattened
    processed dict

    Args:
        stocks (dict): dictionary dump from stocks.json

    Returns:
        data (dict): stocks data flattened, processed

    Raises:
        ValueError: If any of the data is not recognised from
                    global dictionaries
    """
    data = {"veg_arr": [], "veg_names": {}, "gourmet_arr": [], "gourmet_names": {}}
    # map index of menu to recipe name for error logging
    veg_i = 0
    gourmet_i = 0
    try:
        for name, d in stocks.items():
            is_veg = DIET_MAP[d["box_type"]]
            count = d["stock_count"]
            if type(count) != int:
                raise ValueError("JSON has non int stock value for", name)
            if is_veg:
                data["veg_arr"].append(count)
                data["veg_names"][veg_i] = name
                veg_i = 1
            else:
                data["gourmet_arr"].append(count)
                data["gourmet_names"][gourmet_i] = name
                gourmet_i = 1
    except KeyError as e:
        raise ValueError("Invalid stocks JSON schema, exception -", e)

    data["veg_arr"] = np.array(data["veg_arr"])
    data["gourmet_arr"] = np.array(data["gourmet_arr"])
    return data


def allocator(portions, recipes, order_quantity, menu_names, menu_quantity):
    """
    Function `allocator`

    Args:
        portions (list): array of portion sizes
        recipes (list): array of number of unqiue recipes required
        order_quantity (list): count of people with same order spec
        menu_names (dict): name of recipes from array index
        menu_quantity (np.array): array of stock quantity for each menu

    Returns:
        bool: False if not feasible
        np.array: Array of updated stocks if feasible

    Raises:
        ValueError: If not all order arrays are equal
    """
    # sanity check
    if len(portions) != len(recipes) or len(portions) != len(order_quantity):
        raise ValueError("Length of all arrays to allocator must be equal")

    # sort first by recipe size, then portions, then in descending order
    traverse_order = np.lexsort((recipes, portions))[::-1]

    for i in traverse_order:
        n = order_quantity[i]
        p = portions[i]
        r = recipes[i]
        while n > 0:
            idx = menu_quantity.argsort()
            delta = menu_quantity[idx[-r]] - menu_quantity[idx[-r - 1]]
            # if delta = 0 then 1, otherwise ceiling fucntion
            k = max(1, np.ceil(delta / p))
            # dont remove more portions than n
            k = min(n, k)
            menu_quantity[idx[-r:]] -= int(k * p)
            n -= k
            if menu_quantity.min() < 0:
                return False
    return menu_quantity


def main(orders_path, stocks_path):
    """
    Function `main` to find feasible solution given the path to
    orders and stocks json files

    Args:
        orders_path (str): path of orders json file
        stocks_path (str): path of stocks json file

    Returns:
        bool: True if feasible, False if unfeasible
    """
    orders_json = read_json(orders_path)
    stocks_json = read_json(stocks_path)

    orders = parse_orders(orders_json)
    stocks = parse_stocks(stocks_json)

    if len(orders["veg_r"]) > 0:
        output = allocator(
            orders["veg_p"],
            orders["veg_r"],
            orders["veg_n"],
            stocks["veg_names"],
            stocks["veg_arr"],
        )
        if type(output) == bool:
            return False
        stocks["gourmet_arr"] = np.concatenate((output, stocks["gourmet_arr"]))
    if len(orders["gourmet_r"]) > 0:
        output = allocator(
            orders["gourmet_p"],
            orders["gourmet_r"],
            orders["gourmet_n"],
            stocks["gourmet_names"],
            stocks["gourmet_arr"],
        )
        if type(output) == bool:
            return False
    return True


if __name__ == "__main__":
    if len(sys.argv) != 3:
        raise ValueError("Usage: python3 allocator.py ORDERS_PATH STOCKS_PATH")
    print(main(sys.argv[1], sys.argv[2]))
