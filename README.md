# Data Scientist Tests
## Recipe allocator algorithm

Robert Sparks
hire@rsparks.xyz

23/11/20

# Files
* `allocator.py`: recipe allocator algorithm
* `generate_test_json.py`: synthetic data generation script
* `presentation.pdf`: presentation of findings/walkthrough of process
* `test_allocator.py`: unittest module for testing `allocator.py`
* `notebooks`: folder containing draft notebooks used in prototyping,
includes Google OR-tools work
* `tests`: folder containing all test json

# Todo
* finish writing unit tests
* add logs to diagnose faults
