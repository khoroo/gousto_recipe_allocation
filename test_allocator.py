#!/usr/bin/env python3

import allocator
import json
import unittest


class TestAllocator(unittest.TestCase):

    def test_json(self):
        with self.assertRaises(FileNotFoundError):
            allocator.read_json('file_not_found.json')
        with self.assertRaises(json.JSONDecodeError):
            allocator.read_json('tests/test_json_fileerror.json')

    def test_parse_orders(self):
        for t in ('tests/test_json_unsupported_headings__orders.json',
                  'tests/test_json_unexpected_type.json',
                 ):
             dump = allocator.read_json(t)
             with self.assertRaises(ValueError):
                 allocator.parse_orders(dump)
    def test_allocator(self):
        for p in ('small__', 'large__'):
            prefix = 'tests/test_true_'
            orders_path = prefix + p + 'orders.json'
            stocks_path = prefix + p + 'stocks.json'
            self.assertTrue(allocator.main(orders_path, stocks_path))

        self.assertFalse(allocator.main('tests/test_false__orders.json', 'tests/test_false__stocks.json'))




if __name__ == '__main__':
    unittest.main()
