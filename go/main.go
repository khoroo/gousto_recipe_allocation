package main

import (
	"fmt"
	"math"
	"sort"
)

func allocator(portions []int, recipes []int, orders []int, mQ *[]int) {
	order := [...]int{5, 3, 1, 4, 2, 0}
	for _, e := range order {
		n := orders[e]
		p := portions[e]
		r := recipes[e]
		nM := len(*mQ)
		for n > 0 {
			sort.Ints(*mQ)
			delta := (*mQ)[nM-r] - (*mQ)[nM-r-1]
			var k int = 1
			if delta > 0 {
				ceil := math.Ceil(float64(delta) / float64(p))
				k = int(ceil)
			}
			if k > n {
				k = n
			}
			var rm int = int(k * p)
			for i := nM - r; i < nM; i++ {
				ans := (*mQ)[i] - rm
				(*mQ)[i] = ans
			}
			n -= k

		}
	}

}

func main() {

	vOrders := []int{100000, 100000, 100000, 100000, 100000, 100000}
	gOrders := []int{100000, 100000, 100000, 100000, 300000, 100000}

	portions := []int{2, 4, 2, 4, 2, 4}

	recipes := []int{2, 2, 3, 3, 4, 4}

	menuQuant := []int{600000, 400000, 600000, 400000, 400000, 1000000, 1600000, 1000000, 600000, 400000, 800000, 400000, 600000, 400000, 600000, 400000, 200000, 400000, 800000, 400000, 200000, 200000}

	nVeg := 8
	mQV := menuQuant[:nVeg]
	fmt.Println(menuQuant)
	allocator(portions, recipes, vOrders, &mQV)
	allocator(portions, recipes, gOrders, &menuQuant)
	fmt.Println(menuQuant)
}
