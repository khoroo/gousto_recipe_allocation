#!/usr/bin/env python3

"""
quick and dirty synthetic data generation script for allocation algorithm

use:
 $ python3 generate_test_json.py 1 10 22 8 otest stest

robert sparks
23/11/20
"""

import json
import numpy as np
import sys

# seed for repeatability
RNG = np.random.default_rng(42)

def format_stocks(stocks: np.ndarray, n_veg: int):
    recipes = {}
    diet = 'vegetarian'
    for i, num in enumerate(stocks):
        if i >= n_veg:
            diet = 'gourmet'
        recipes[f'recipe_{i + 1}'] = {'stock_count': int(num),
                                      'box_type': diet}
    return recipes

def format_orders(orders: np.ndarray):
    recipe_map = {4:'four_recipes', 3:'three_recipes', 2:'two_recipes'}
    portion_map = {4:'four_portions', 2:'two_portions'}
    key = 'vegetarian'
    out = {'vegetarian': {'two_recipes':{}, 'three_recipes':{},
                             'four_recipes':{}},
              'gourmet': {'two_recipes':{}, 'three_recipes':{},
                          'four_recipes':{}}
             }
    for i, row in enumerate(orders):
        # orders array is veg for 6 rows, then gourmet for 6 hence
        if i > 5:
            key = 'gourmet'
        recp = recipe_map[row[0]]
        port = portion_map[row[1]]
        out[key][recp][port] = int(row[2])
    return out


def synthesise_exact_stocks(stocks: np.ndarray, orders: np.ndarray):
    for row in orders:
        for _ in range(row[2]):
            stocks[:row[0]] += row[1]
            RNG.shuffle(stocks)
    return stocks

def dump_json(fname: str, out: dict):
    with open(fname + '.json', 'w') as fp:
        json.dump(out, fp,  indent=4)

def main(lower_orders, upper_orders, n_menus, n_veg, orders_file, stocks_file):
    orders = np.zeros((12, 3), dtype=np.int32)
    orders[:, 0] = (4, 4, 3, 3, 2, 2) * 2
    orders[:, 1] = (4, 2) * 6
    orders[:, 2] = RNG.integers(lower_orders, high=upper_orders, size=12)

    n_menus = 22
    n_veg = 8
    stocks = np.zeros(n_menus)
    veg_stocks = synthesise_exact_stocks(stocks[:n_veg], orders[:6, :])
    stocks = synthesise_exact_stocks(stocks, orders[6:, :])
    stocks[:n_veg] += veg_stocks

    dump_json(orders_file, format_orders(orders))
    dump_json(stocks_file, format_stocks(stocks, n_veg))




if __name__ == '__main__':
    lower_orders, upper_orders, n_menus, n_veg, orders_file, stocks_file = sys.argv[1:]
    main(
        int(lower_orders),
        int(upper_orders),
        int(n_menus),
        int(n_veg),
        orders_file,
        stocks_file
        )
